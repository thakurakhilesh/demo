package com.example.myapplication;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main2Activity_SignUp extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    EditText etName;
    EditText etEmail;
    EditText etPassword;
    EditText etPhone;
    Button btSignup;
    RadioGroup rg;
    RadioButton rbMale;
    RadioButton rbFemale;
    Spinner spInterests;
    TextView tvContainer;
    CheckBox cbActing, cbPlaying, cbDancing;
    String Gender;
    Dialog dialog;
    pref p;
    String Name,Email,Mobile,interests;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2__sign_up);
        findId();
        p=new pref(this);

    }

    private void findId() {

        tvContainer = (findViewById(R.id.tvcontainer));
        spInterests = (findViewById(R.id.spInterests));
        etName = findViewById(R.id.etname);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPass);
        etPhone = findViewById(R.id.etMobile);
        btSignup = findViewById(R.id.btSignup);
        rg = findViewById(R.id.rg);
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        cbActing = findViewById(R.id.cbActing);
        cbPlaying = findViewById(R.id.cbPlaying);
        cbDancing = findViewById(R.id.cbDancing);
        spinnerItems();

        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignUp(v);
            }
        });

    }

    private void onSignUp(View v) {
         Name = etName.getText().toString().trim();
         Email = etEmail.getText().toString().trim();
         String Password = etPassword.getText().toString().trim();
         Mobile = etPhone.getText().toString().trim();
         String spinnerFirst = (String) spInterests.getItemAtPosition(0);
        String Hobbies = hobbiesChecked();
        int selectedId = rg.getCheckedRadioButtonId();


        spInterests.setOnItemSelectedListener(this);

        if (Name.isEmpty() || Email.isEmpty() || Password.isEmpty() || Mobile.isEmpty()) {
            util.toast(this, "All fields are required");
        } else if (Name.length() < 4) {

            etName.setError("Invalid Name");
        } else if (Email.length() < 8 || !Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {

            etEmail.setError("Invalid Email");
        } else if (Password.length() < 6 || !isValidPassword(Password)) {
            etPassword.setError("invalid password");
        } else if (Mobile.length() < 10 || !Patterns.PHONE.matcher(Mobile).matches()) {

            etPhone.setError("Invalid Phone");
        } else if (selectedId == -1) {
            rg.setFocusable(true);
            util.toast(this, "Please select a gender");
        } else if (Hobbies == "") {
            util.toast(this, "please select a hobby");
        } else if (spInterests.getSelectedItem().toString().trim().equals(spinnerFirst)) {
            util.toast(this, "Select a interest");
        } else {


            openRegisterDialog();
            //    util.toast(this, "User Registered successfully");


            switch (selectedId) {
                case R.id.rbMale:
                    Gender = (String) rbMale.getText();
                    //  util.toast(this, (String) rbMale.getText());
                    break;
                case R.id.rbFemale:
                    Gender = (String) rbFemale.getText();
                    // util.toast(this, (String) rbFemale.getText());
            }
//
//                    User user = new User(Name, Email, Password, Mobile, Gender, Hobbies, Interests);
//                    user.setName(Name);
//                    user.setEmail(Email);
//                    user.setGender(Gender);
//                    user.setMobile(Mobile);
//                    user.setPassword(Password);
//                    user.setHobbies(Hobbies);
//                    user.setInterests(Interests);
//
//
//                    //getter
//                    util.toast(this,user.getEmail());
//
//
        }
//


    }


    private String hobbiesChecked() {
        String hobbies = "";
        if (cbActing.isChecked()) {
            hobbies += "Acting";
        }
        if (cbPlaying.isChecked()) {
            hobbies += "Playing";
        }
        if (cbDancing.isChecked()) {
            hobbies += "Dancing";
        }
        // util.toast(this,hobbies);
        return hobbies;
    }


    private boolean isValidPassword(String password) {
        Pattern pattern;
        Matcher matcher;
        String password_matcher = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(password_matcher);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private void spinnerItems() {

        ArrayList<String> spinner = new ArrayList<String>();
        spinner.add("-Select-");
        spinner.add("Sports");
        spinner.add("Traveling");
        spinner.add("Coding");
        spinner.add("Designing");
        spinner.add("Reading");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, spinner);
        spInterests.setAdapter(dataAdapter);
        tvContainer.setMovementMethod(new ScrollingMovementMethod());


    }

    private void openRegisterDialog() {
        //  util.toast(Main2Activity_SignUp.this, "On Custom dialog");
        dialog = new Dialog(Main2Activity_SignUp.this);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button Yes = dialog.findViewById(R.id.btnYes);
        Button No = dialog.findViewById(R.id.btnNo);
        dialog.show();
        Yes.setOnClickListener(Main2Activity_SignUp.this);
        No.setOnClickListener(Main2Activity_SignUp.this);


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.btnYes:
                Yes();
            case R.id.btnNo:
                No();
        }
    }

    private void No() {
        util.toast(this, "Registration Denied");
        dialog.dismiss();
    }

    private void Yes() {
        p.setValue(Name,Email,Mobile,interests,Gender);
        p.setLogin(true);
        startActivity(new Intent(getApplicationContext(),home.class));
        util.toast(this, "User Registred successfully");
        dialog.dismiss();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        interests= (String) parent.getItemAtPosition(position);


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//        util.toast(this,"nothing is selected");
//
//    }
}
