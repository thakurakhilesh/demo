package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class listview extends AppCompatActivity {
    ListView lv_list;
    Button btnlogout;
    pref p;
    ArrayList<String> list;
    ArrayList<String> spec;
    ArrayList<Integer> image;
    ArrayList<String> moreinfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        lv_list = findViewById(R.id.lvlist);
        btnlogout=findViewById(R.id.btnlogout);
        p = new pref(this);




        nameList();
        specList();
        imageList();
        moreinfo();


        CustomAdapter adapter = new CustomAdapter(this, list, image, spec, moreinfo);
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.setLogin(false);
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });
        lv_list.setAdapter(adapter);
        }

    private void moreinfo() {
        moreinfo= new ArrayList<String>();
        moreinfo.add(getString(R.string.rohit));
        moreinfo.add(getString(R.string.shikhar));
        moreinfo.add(getString(R.string.virat));
        moreinfo.add(getString(R.string.Dhoni));
        moreinfo.add(getString(R.string.Rahane));
        moreinfo.add(getString(R.string.kartik));
        moreinfo.add(getString(R.string.jadeja));
        moreinfo.add(getString(R.string.kuldeep));
        moreinfo.add(getString(R.string.bumrah));
        moreinfo.add(getString(R.string.bhuvi));
        moreinfo.add(getString(R.string.shami));
    }

    private void imageList() {
        image = new ArrayList<>();
        image.add(R.drawable.rohit);
        image.add(R.drawable.shikhar);
        image.add(R.drawable.virat);
        image.add(R.drawable.msd);
        image.add(R.drawable.ajinkya);
        image.add(R.drawable.dinesh);
        image.add(R.drawable.ravindra);
        image.add(R.drawable.kuldeep);
        image.add(R.drawable.bumrah);
        image.add(R.drawable.bhuvi);
        image.add(R.drawable.shami);

    }

    private void specList() {
        spec = new ArrayList<>();
        spec.add("Right Hand Bat(VC)");
        spec.add("Left Hand Bat");
        spec.add("Right Hand Bat(C)");
        spec.add("Right Hand Bat(WK)");
        spec.add("Right Hand Bat");
        spec.add("Right Hand Bat");
        spec.add("Spin Alrounder");
        spec.add("Left-arm chinaman Bowler");
        spec.add("Right-arm fast-medium Bowler");
        spec.add("Right-arm fast-medium Bowler");
        spec.add("Right-arm fast-medium Bowler");

    }

    private void nameList() {
       list = new ArrayList<>();
        list.add("Rohit Sharma");
        list.add("Shikhar Dhawn");
        list.add("Virat Kohli");
        list.add("MS Dhoni");
        list.add("Ajinkya Rahane");
        list.add("Dinesh Kartik");
        list.add("Ravindra Jadeja");
        list.add("Kuldeep Yadav");
        list.add("Jasprit Bumrah");
        list.add("Bhuvneshwar Kumar");
        list.add("Mohamad Shami");

    }

}
