package com.example.myapplication;

import android.content.Context;
import android.content.SharedPreferences;

public class pref {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public String key = "isLogin";


    pref(Context context) {
        sharedPreferences = context.getSharedPreferences("Shared Preference", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public boolean setLogin(Boolean status) {
        editor.putBoolean(key, status);

        editor.apply();
        editor.commit();
        editor.apply();
        return false;
    }

    public boolean getLogin() {

        return sharedPreferences.getBoolean(key, false);
    }

    public void setValue(String name,String email,String password,String mobile,String Gender) {
        editor.putString("name", name);
        editor.putString("email",email);
        editor.putString("password",password);
        editor.putString("mobile",mobile);
        editor.putString("gender",Gender);
        editor.commit();
        editor.apply();

    }

    public String getName() {

        return sharedPreferences.getString("name", "");

    }

    public String getEmail()
    {

        return sharedPreferences.getString("email","");
    }
    public String getPassword()
    {

        return sharedPreferences.getString("password","");
    }
    public String getMobile()
    {

        return sharedPreferences.getString("mobile","");
    }

    public String getGender()
    {

        return sharedPreferences.getString("gender","");
    }



}
