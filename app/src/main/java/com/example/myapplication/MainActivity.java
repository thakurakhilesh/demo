package com.example.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_user, et_pass;
    Button btn_login, btn_signup;
    pref p;
    String username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_user = (EditText) findViewById(R.id.etuser);
        et_pass = (EditText) findViewById(R.id.etpass);
        btn_login = (Button) findViewById(R.id.btlogin);
        btn_signup = (Button) findViewById(R.id.btSignup);
        btn_login.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        p = new pref(getApplicationContext());
                /*Radio button-Gender
                Check Box
                circular image
                Spinner-interest
                */


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btlogin:


                login();
                break;
            case R.id.btSignup:
                signup();
                break;
        }


    }

    private void signup() {
        Intent i = new Intent(getApplicationContext(), Main2Activity_SignUp.class);
        startActivity(i);
    }

    private void login() {

      username = et_user.getText().toString().trim();
         password = et_pass.getText().toString().trim();
        onLogin(username, password);

    }

    private void onLogin(String username, String password) {

        if (username.isEmpty() || password.isEmpty()) {
            util.toast(this, "All fields are required");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(username).matches() || username.length() < 8) {
            util.toast(this, "Email patterns is not valid");
        } else if (password.length() < 6) {
            util.toast(this, "Password length must be greater than 6");
        } else if (username.equals("akhilesh@gmail.com") && password.equals("thakur")) {

            openDialog();
            //util.toast(this,"You login as"+username);
        } else {
            util.toast(this, "Wrong username & Password");
        }
    }

    private void openDialog() {
        final AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login");
        builder.setMessage("Do You want to Login?");

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                util.toast(MainActivity.this, "Login Unsuccessfull");
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                util.toast(MainActivity.this, "Login Successfull");
                Intent intent = new Intent(getApplicationContext(), listview.class);
                startActivity(intent);
                p.setLogin(true);

                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}
