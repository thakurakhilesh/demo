package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class gridview extends AppCompatActivity {

    GridView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
        gv = findViewById(R.id.gv);

        ArrayList<String> list = new ArrayList<>();
        list.add("Rohit Sharma");
        list.add("Shikhar Dhawn");
        list.add("Virat Kohli");
        list.add("MS Dhoni");
        list.add("Ajinkya Rahane");
        list.add("Dinesh Kartik");
        list.add("Ravindra Jadeja");
        list.add("Kuldeep Yadav");
        list.add("Jasprit Bumrah");
        list.add("Bhuvneshwar Kumar");
        list.add("Mohamad Shami");

        ArrayList<String> spec = new ArrayList<>();
        spec.add("Right Hand Bat(VC)");
        spec.add("Left Hand Bat");
        spec.add("Right Hand Bat(C)");
        spec.add("Right Hand Bat(WK)");
        spec.add("Right Hand Bat");
        spec.add("Right Hand Bat");
        spec.add("Spin Alrounder");
        spec.add("Left-arm chinaman Bowler");
        spec.add("Right-arm fast-medium Bowler");
        spec.add("Right-arm fast-medium Bowler");
        spec.add("Right-arm fast-medium Bowler");

        ArrayList<Integer> image = new ArrayList<>();
        image.add(R.drawable.rohit);
        image.add(R.drawable.shikhar);
        image.add(R.drawable.virat);
        image.add(R.drawable.msd);
        image.add(R.drawable.ajinkya);
        image.add(R.drawable.dinesh);
        image.add(R.drawable.ravindra);
        image.add(R.drawable.kuldeep);
        image.add(R.drawable.bumrah);
        image.add(R.drawable.bhuvi);
        image.add(R.drawable.shami);

        ArrayList<String> city=new ArrayList<String>();

        city.add("Mumbai");
        city.add("Delhi");
        city.add("Delhi");
        city.add("Ranchi");
        city.add("Mumbai");
        city.add("Chennai");
        city.add("Saurashtra");
        city.add("Kanpur");
        city.add("Ahmedabad");
        city.add("Meerut");
        city.add("Jonagar");

        ArrayList<String> team = new ArrayList<String>();

        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        team.add("India");
        gridcustomadapter adapter = new gridcustomadapter(this, list, image, spec,city,team);
        gv.setAdapter(adapter);
    }
}
