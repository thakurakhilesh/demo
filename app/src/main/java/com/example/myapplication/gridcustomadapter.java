package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class gridcustomadapter extends BaseAdapter {
    static Context context;
    ArrayList<String> list, spec,city,team;
    ArrayList<Integer> image;
    Dialog dialog;

    gridcustomadapter(Context context, ArrayList<String> list, ArrayList<Integer> image, ArrayList<String> spec, ArrayList<String> city,ArrayList<String> team) {
        this.context = context;
        this.list = list;
        this.spec = spec;
        this.image = image;
        this.city=city;
        this.team=team;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.view, parent, false);
        TextView tvName, tvSpec;
        ImageView ivProfile;
        tvName = convertView.findViewById(R.id.tvName);
        tvSpec = convertView.findViewById(R.id.tvspec);
        ivProfile = convertView.findViewById(R.id.ivprofile);
        tvName.setText(list.get(position));
        tvSpec.setText(spec.get(position));
        ivProfile.setImageResource(image.get(position));
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a;
                switch (position) {
                    case 0:

                        a = 0;
                        zoomImage(a);
                        break;
                    case 1:
                        a = 1;
                        zoomImage(a);

                        break;
                    case 2:
                        a = 2;
                        zoomImage(a);

                        break;
                    case 3:
                        a = 3;
                        zoomImage(a);

                        break;
                    case 4:
                        a = 4;
                        zoomImage(a);
                        break;
                    case 5:
                        a = 5;
                        zoomImage(a);

                        break;
                    case 6:
                        a = 6;
                        zoomImage(a);

                        break;
                    case 7:
                        a = 7;
                        zoomImage(a);

                        break;
                    case 8:
                        a = 8;
                        zoomImage(a);

                        break;
                    case 9:
                        a = 9;
                        zoomImage(a);

                        break;
                    case 10:
                        a = 10;
                        zoomImage(a);

                        break;
                }
            }
        });
        return convertView;
    }

    private void zoomImage(int a) {
//        ArrayList<String> name = new ArrayList<String>();
//
//
//
//
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//        name.add("Rohit Sharma");
//
//

//

//

//

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.view1);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvName, tvSpec, tvCity, tvTeam;
        Button btnOK;
        tvName = dialog.findViewById(R.id.tvName);
        tvSpec = dialog.findViewById(R.id.tvSpec);
        tvCity = dialog.findViewById(R.id.tvCity);
        tvTeam = dialog.findViewById(R.id.tvTeam);
        btnOK = dialog.findViewById(R.id.btnOK);
        tvName.setText(list.get(a));
        tvSpec.setText(spec.get(a));
        tvCity.setText(city.get(a));
        tvTeam.setText(team.get(a));
        dialog.show();
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
}
