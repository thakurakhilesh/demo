package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> list, moreinfo;
    ArrayList<Integer> image;
    ArrayList<String> spec;
    Dialog dialog;

    public CustomAdapter(listview listview, ArrayList<String> list, ArrayList<Integer> image, ArrayList<String> spec, ArrayList<String> moreinfo) {
        context = listview;
        this.list = list;
        this.image = image;
        this.spec = spec;
        this.moreinfo = moreinfo;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.view, parent, false);
        final TextView tvName;
        final TextView tvspec;
        final ImageView improfile;


        tvName = view.findViewById(R.id.tvName);
        tvspec = view.findViewById(R.id.tvspec);
        improfile = view.findViewById(R.id.ivprofile);

        tvName.setText(list.get(position));
        tvspec.setText(spec.get(position));
        improfile.setImageResource(image.get(position));

        improfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a;
                switch (position) {
                    case 0:
                        Intent i = new Intent(context, gridview.class);
                        context.startActivity(i);
//                        a = 0;
//                        zoomImage(a);
                        break;
                    case 1:
                        a = 1;
                        zoomImage(a);

                        break;
                    case 2:
                        a = 2;
                        zoomImage(a);

                        break;
                    case 3:
                        a = 3;
                        zoomImage(a);

                        break;
                    case 4:
                        a = 4;
                        zoomImage(a);
                        break;
                    case 5:
                        a = 5;
                        zoomImage(a);

                        break;
                    case 6:
                        a = 6;
                        zoomImage(a);

                        break;
                    case 7:
                        a = 7;
                        zoomImage(a);

                        break;
                    case 8:
                        a = 8;
                        zoomImage(a);

                        break;
                    case 9:
                        a = 9;
                        zoomImage(a);

                        break;
                    case 10:
                        a = 10;
                        zoomImage(a);

                        break;
                }
                //util.toast(context, String.valueOf(position));
            }
        });

        return view;
    }

    public void zoomImage(int a) {

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.imagebox);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView iv;
        TextView tvName, tvSpec;
        TextView tvMore;
        Button btnOK;
        tvName = dialog.findViewById(R.id.tvName);
        tvSpec = dialog.findViewById(R.id.tvSpec);
        tvMore = dialog.findViewById(R.id.tvMore);
        iv = dialog.findViewById(R.id.ivShow);
        btnOK = dialog.findViewById(R.id.btnOk);
        iv.setImageResource(image.get(a));
        tvName.setText(list.get(a));
        tvSpec.setText(spec.get(a));
        tvMore.setText(moreinfo.get(a));
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

}
