package com.example.myapplication;

public class User {
    private String Name;
    private String Email;
    private String Password;
    private String Mobile;
    private String Gender;
    private  String Hobbies;
    private String Interests;

    public User(String name, String email, String password, String mobile, String gender, String hobbies, String interests) {

        this.Name=name;
        this.Email=email;
        this.Password=password;
        this.Mobile=mobile;
        this.Gender=gender;
        this.Hobbies=hobbies;
        this.Interests=interests;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getInterests() {
        return Interests;
    }

    public void setInterests(String interests) {
        Interests = interests;
    }
}
