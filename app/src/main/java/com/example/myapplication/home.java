package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class home extends AppCompatActivity {

    TextView name,email,mobile,interests;
    pref p;
    String Name,Email,Mobile,Interests;
    Button btnlogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        interests=findViewById(R.id.interests);
        btnlogout=findViewById(R.id.btnlogout);
         p=new pref(this);

         Name=p.getName();
         Email=p.getEmail();
         Mobile=p.getMobile();
         Interests=p.getPassword();

name.setText(Name);
email.setText(Email);
mobile.setText(Mobile);
interests.setText(Interests);
btnlogout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        p.setLogin(false);
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
});
    }
}
